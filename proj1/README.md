# Proj1

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Group Members:

Arnav Aima - 99864675
Devanshu Singh - 65131155

Problem Description:

N = 1000000, k = 4

1. Size of worker unit: 2000 sub-problems/worker. We settled at this value after thoroughly investigating what batch size gives us best performance.

2. Executing [mix run proj1.exs 1000000 4] does not give any output as no solution exists for the given values of N and k.

3. Running time for the above mentioned execution is as follows:
   
   121.09 user, 1.27 system and 0:31.12 elapsed 393%CPU

   CPU time : Real time = (user+system)/elapsed = 122.36/31.12 = 3.93

4. Largest problem solved is N = 10000000, k = 409
   
   Solution for the problem:

   71752
   1236640

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Code execution:

  mix compile
  mix run proj1.exs N k