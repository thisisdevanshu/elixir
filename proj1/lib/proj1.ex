defmodule Proj1 do
  @moduledoc """
  A module that implements Supervisor to start and track child actors performing mathematical calculations.
  """

  use Supervisor

  @doc """
  Convert args to integers and call method to start Supervisor.
  """
  def start(arg_one, arg_two) do
    n = String.to_integer(arg_one)
    k = String.to_integer(arg_two)

    start_worker({1,n,k})

  end

  @doc """
  Override default Supervisor init function.
  """
  def init(n) do
    {:ok, n}
  end

  @doc """
  Initialize list of child actors, assign work units for each actor and start child actors.
  """
  def start_worker({s,n,k}) do

    range = if s + 2000 > n do
      Enum.to_list  s..n
    else
      Enum.to_list  s..(s + 2000)
    end
    children = Enum.map(range, fn(i) ->
      worker(Child, [{i,k}], [ id: i, restart: :transient])
    end)
    Supervisor.start_link(children, [strategy: :one_for_one, max_restarts: 1])

    if n > s + 2000 do 
      start_worker({s + 2001, n, k})
    end
  end

end

defmodule Child do
  @moduledoc """
   module that implements child actors using Task and does the required mathematical calculations to check and print valid square series.
  """
  use Task
  def start_link({n,k}) do
    Task.start_link(__MODULE__, :run, [{n,k}])
  end

  def run({n,k}) do
    sq(n, n + k - 1)
  end

  @doc """
  Calculate sum of squares over range and check for perfect square number existence for the same. Finally, print first number in series if required condition met. 
  """
  def sq(n, k) do
    range = Enum.to_list  n..(k)
    sum = Enum.reduce(range, 0, fn (i, acc) -> i * i + acc end)
    sqrt = :math.sqrt(sum)
    sqrt = Kernel.trunc(sqrt)
    if (sqrt*sqrt == sum) do
      IO.puts n
    end
  end 

end


