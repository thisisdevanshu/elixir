defmodule Bitcoin do

  use GenServer

  def start(transactor, wallets,n) do
    {:ok , propagator} = Propagator.start_link()

    nonce = 1
    map = %{} 
    map = initiate_blockchain(1, n, propagator, map)
    
    GenServer.cast( transactor, {:add_network, map})
    Enum.map(map, fn({id, pid}) -> GenServer.cast(pid,{:update_transactor, transactor}) end)
    Enum.map(map, fn({id, pid}) -> add_wallet(id,pid,wallets) end)
    GenServer.cast( propagator, {:update_map, map, 3})

    {id,pid} = Map.fetch(map, 1)
    GenServer.cast( pid, {:mine_block, nonce})
  end


  def initiate_blockchain(i, num_nodes, propagator, map) do
      if i < num_nodes do
        {:ok, pid} = Block.start_link(propagator)
        updated_map = Map.put(map,i, pid)
        updated_map = initiate_blockchain(i+1, num_nodes, propagator, updated_map)
        updated_map
      else
        map
      end
  end

  def add_wallet(id, pid, wallets) do
    {:ok, wallet} = Map.fetch(wallets,id)
    GenServer.cast(pid, {:update_wallet, wallet})
  end

end


#====================================================================================
defmodule Block do

  use GenServer

  def start_link(args) do
    GenServer.start_link(__MODULE__, [args] )
  end

  def init(args) do
    test = :crypto.hash(:sha256, "abc") |> Base.encode16
    {:ok, private_key} = RsaEx.generate_private_key
    {:ok, public_key} = RsaEx.generate_public_key(private_key)
    [propagator] = args 
    time = :os.system_time(:millisecond)
    time = Integer.to_string(time)
    previous_hash = "hash"
    data = []
    chain = []
    public_key = []
    transactor = []
    :timer.sleep(100)
    state = [propagator,  time, previous_hash, data, chain, public_key, transactor]
    {:ok, state}
  end

  def handle_cast({:mine_block, nonce}, state) do
    [propagator, time, previous_hash, data, list, public_key, transactor] = state
     
    difficulty = "00000"
    block = "#{previous_hash}#{time}#{nonce}#{data}"
    hash = :crypto.hash(:sha256, block) |> Base.encode16
    if  String.equivalent?( String.slice(hash, 0,5), difficulty)  do
        #new_data = data ++ [25]
        new_block = {hash, time , nonce, data}
        if Enum.member?(list, hash) do
          IO.puts "already exist"
        else
          IO.inspect new_block, label: "Generated Block"
          Block.broadcast(new_block,propagator)
          #IO.inspect public_key
          GenServer.cast(transactor, { :add_transaction , hash, public_key})
        end 
    else
        nonce = nonce + 1
        mine_block(nonce)
    end
    {:noreply, state}
  end

  def mine_block( nonce) do
    GenServer.cast(self(), {:mine_block, nonce})
  end

  def broadcast(new_block, propagator) do
    GenServer.cast( propagator, {:propagate, new_block} )
  end

  def handle_cast({:extend_blockhain, new_block, new_time},state) do

      {hash, time , nonce, new_data} = new_block
      [propagator, o_time, previous_hash, data,list,public_key, transactor] = state

      extend = is_valid(hash, time, previous_hash, data,nonce )

      previous_hash = if extend do
        hash
      else
        previous_hash
      end

      list = if extend do
        list ++ [{hash, time , nonce, new_data}]
      else
        list
      end

      time = if extend do
        new_time
      else
        time
      end
    
      data = []

      state = [propagator, time, previous_hash, data, list, public_key, transactor]

      {:noreply, state}
  end
  
  def handle_cast({:update_data, new_data}, state ) do
    [propagator, time, previous_hash, data,list, public_key, transactor] = state
    data = data ++ [new_data]
    state = [propagator, time, previous_hash, data,list,public_key, transactor]
    {:noreply, state}
  end

   def handle_cast({:update_wallet, public}, state ) do
    [propagator, time, previous_hash, data,list, public_key, transactor] = state
    public_key = public
    state = [propagator, time, previous_hash, data,list,public_key, transactor]
    {:noreply, state}
  end

  def handle_cast({:update_transactor, new_transactor}, state ) do
    [propagator, time, previous_hash, data,list, public_key, transactor] = state
    transactor = new_transactor
    state = [propagator, time, previous_hash, data,list,public_key, transactor]
    {:noreply, state}
  end

  def is_valid(hash, timestamp, previous_hash, data, nonce) do
    block = "#{previous_hash}#{timestamp}#{nonce}#{data}"
      new_hash = :crypto.hash(:sha256, block) |> Base.encode16
      hash == new_hash
  end

end

#========================================================================
defmodule Propagator do

  use GenServer

  import Kernel

  def start_link do
    GenServer.start_link(__MODULE__, [])
  end

  def init(args) do
      {:ok, args}
  end

  def propagate(new_block, propagator) do
    GenServer.cast(self(), {:propagate, new_block})
  end

  def handle_cast({:propagate, new_block}, state) do
    [map, size] = state
    new_time = :os.system_time(:millisecond)
    new_time =  Integer.to_string(new_time)
    Enum.map(map, fn({id, pid}) -> GenServer.cast(pid,{:extend_blockhain, new_block, new_time}) end)
    Enum.map(map, fn({id, pid}) -> GenServer.cast(pid,{:mine_block, 1}) end)
    {:noreply, state}
  end


  def handle_cast({:update_map, map, size}, state) do
    state = [map, size]
    {:noreply, state}
  end

end

#=================================================================================
defmodule Transaction do
  
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, [])
  end

  def init(args) do
      pending_transactions = []
      transactions = %{}
      wallets = %{}
      network = []
      state = [pending_transactions, transactions, network, wallets]
      {:ok, state}
  end

  def handle_cast({:add_network, map} , state) do
    [pending_transactions, transactions, network, wallets] = state
    network = map
    state = [pending_transactions, transactions, network, wallets]
    {:noreply, state}
  end

  def handle_cast({:add_transaction, hash, pid }, state) do
    [pending_transactions, transactions, network, wallets] = state
    #update the balance of all wallets
    Enum.map(pending_transactions, fn([public_key, recipient_key, amount]) ->
      update_wallet(wallets, public_key, recipient_key, amount) end)
    #update the transaction list with hash of block
    #IO.inspect pid, label: "mined coin fn"
    transactions = Map.put(transactions, hash, pending_transactions)
    #flush pending transaction
    wallet = GenServer.call(pid, :get_public_key)
    pending_transactions = [ [0, wallet, 25]]
    GenServer.cast(self(),{:add_pending_transaction, "mined", 0, wallet, 25})
    state = [pending_transactions, transactions, network, wallets]
    #IO.inspect transactions
    {:noreply, state}
  end

  def update_wallet(wallets, public_key, recipient_key, amount) do
        
        {:ok, to_wallet}  = Map.fetch(wallets, recipient_key)

        if public_key != 0 do
          {:ok, from_wallet} = Map.fetch(wallets, public_key)
          balance = GenServer.call(from_wallet, :get_balance)
          if(balance >= amount) do
            GenServer.cast(from_wallet, {:receive, balance - amount})
          end
        end
        #IO.puts "#{public_key} #{balance - amount}"
        GenServer.cast(to_wallet, {:receive, amount})
        #IO.puts "#{recipient_key} #{amount}"
  end
  

  def handle_cast({:add_pending_transaction, signature, public_key, recipient_key, amount }, state) do
    [pending_transactions, transactions, network, wallets] = state
    #verify the amount
    #update data on nodes
    Enum.map(network, fn({id, pid}) -> GenServer.cast(pid,{:update_data, [public_key, recipient_key, amount]}) end)
    pending_transactions = pending_transactions ++ [[public_key, recipient_key, amount]]
    state = [pending_transactions, transactions, network, wallets]
    {:noreply, state}
  end

  def handle_cast({:update_map, public_key, wallet}, state) do
    #add every wallet in map with public key as key
    [pending_transactions, transactions, network, wallets] = state
    wallets = Map.put(wallets, public_key, wallet )
    state = [pending_transactions, transactions, network, wallets]
    {:noreply, state}
  end

end
#==================================================================================
defmodule Wallet do
  
  use GenServer

  def start_link(args) do
    GenServer.start_link(__MODULE__, [args])
  end

  def init(args) do
      [transactor] = args 
      {public, private} = :crypto.generate_key(:ecdh, :secp256k1)
      public_key = Base.encode16(public)
      private_key = Base.encode16(private)
      balance = 0
      GenServer.cast(transactor, {:update_map, public_key, self()})
      state = [public_key, private_key, balance, transactor]
      {:ok, state}
  end

  def send(recipient_key, amount) do
    GenServer.cast(self(), {:send, recipient_key, amount})
  end

  def handle_cast({:send, recipient_key, amount},state) do
    [public_key, private_key, balance, transactor] = state
    if balance >= amount do
      message = "#{amount}"
      signature = []#:crypto.sign(:ecdsa,:sha256, message, [private_key, :secp256k1])
      GenServer.cast(transactor, {:add_pending_transaction, signature, public_key, recipient_key, amount})
    else
      IO.puts "Amount cannot be more than wallet balance"
    end
    {:noreply, state}
  end

  def receive(sender_key, amount) do
    GenServer.cast(self(), {:receive, amount})
  end

  def handle_cast({:receive,  amount},state) do
    [public_key, private_key, balance, transactor] = state
    balance = amount
    #IO.inspect "#{public_key} has #{balance} BTC"
    state = [public_key, private_key, balance, transactor]
    {:noreply, state}
  end

   def handle_cast({:mined_coin},state) do
    [public_key, private_key, balance, transactor] = state
    balance = balance + 25
    #IO.inspect "#{public_key} has #{balance} BTC"
    state = [public_key, private_key, balance, transactor]
    {:noreply, state}
  end

  def handle_cast({:update_transactor, new_transactor}, state ) do
    [public_key, private_key, balance, transactor] = state
    transactor = new_transactor
    state = [public_key, private_key, balance, transactor]
    {:noreply, state}
  end

  #get key method
  def handle_call(:get_public_key, _from, state) do
    [public_key, private_key, balance, transactor] = state
    {:reply, public_key, state}
  end

  def handle_call(:get_balance, _from, state) do
    [public_key, private_key, balance, transactor] = state
    {:reply, balance, state}
  end

  def handle_cast({:set_balance, new_balance}, state ) do
    [public_key, private_key, balance, transactor] = state
    balance = new_balance
    state = [public_key, private_key, balance, transactor]
    {:noreply, state}
  end


end
#=======================================================================
defmodule Proj4 do

def initiate_wallets(i, n, map, transactor) do
      if i < n do
        {:ok, pid} = Wallet.start_link(transactor)
        updated_map = Map.put(map,i, pid)
        updated_map = initiate_wallets(i+1, n, updated_map, transactor)
        updated_map
      else
        map
      end
end

end
