# escript to fetch command-line arguments (number of nodes, topology, algorithm) and start supervisor.
# topologies: full, 3D, rand2D, sphere, line, imp2D
# algorithm: gossip, push-sum

num_nodes = Enum.at(System.argv, 0)
topology = Enum.at(System.argv, 1)
algorithm = Enum.at(System.argv, 2)

num_nodes = String.to_integer(num_nodes)

Proj2.start(num_nodes, topology, algorithm)