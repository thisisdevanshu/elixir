defmodule Proj2 do
  
  def start(nodes, topology, algorithm, convergence) do
    num_nodes =
      cond do
        topology == "2D" or topology == "imp2D" or topology == "torus" ->
          sqrt = Float.ceil(:math.sqrt(nodes))
          round(:math.pow(sqrt, 2))
        topology == "3D" ->
          cubert = Float.ceil(:math.pow(nodes, 0.33))
          round(:math.pow(cubert, 3))
        true ->
          nodes
      end
    IO.inspect num_nodes, label: "Node count"
    args = {num_nodes, topology, algorithm, convergence}
    Controller.start_link(args)
  end

end

#=========================================
defmodule Controller do
  
  use Supervisor

  def start_link(args) do
    num_nodes = elem(args, 0)
    topology = elem(args, 1)
    convergence = elem(args, 3)
    range  = 1..num_nodes

    actor_type =
      cond do
        elem(args, 2) == "gossip" ->
          GossipActor
        elem(args, 2) == "push-sum" ->
          PushSumActor
        true ->
          IO.puts "Invalid algorithm."
      end
    if(actor_type == GossipActor) do
    children = Enum.map(range, fn(i) ->
      worker(actor_type, [{i}], [ id: i, restart: :transient])
    end)   
    {:ok , controller} = Supervisor.start_link(children, strategy: :one_for_one)
    {:ok , pid} = Terminator.start_link()
    start_time = :os.system_time(:millisecond)
    IO.inspect start_time, label: "Start Time"
    GossipActor.rumor(1, num_nodes, topology,pid,controller,start_time, convergence)
  else
    children = Enum.map(range, fn(i) ->
      worker(actor_type, [{i}], [ id: i, restart: :transient])
    end)   
    {:ok , controller} = Supervisor.start_link(children, strategy: :one_for_one)
    {:ok , pid} = Terminator.start_link()
    start_time = :os.system_time(:millisecond)
    IO.inspect start_time, label: "Start Time"
    PushSumActor.rumor(1, num_nodes, topology,pid,controller,start_time, [0, 0], convergence)
  end

  end

  def init(args) do
    {:ok, args}
  end

end
#===========================================
defmodule Terminator do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, [])
  end

  def check_termination(pid,num_nodes,controller,start_time, convergence) do
    GenServer.cast(pid,{:check_termination, num_nodes,controller,pid,start_time, convergence})
  end

  def handle_cast({:check_termination, num_nodes,_controller,pid,start_time, convergence}, state) do
    state = state + 1
    convergence = 0.01*convergence
    if state >= (convergence * num_nodes) do
      end_time = :os.system_time(:millisecond)
      IO.inspect end_time, label: "End Time"
      IO.inspect end_time - start_time, label: "Network Converged in"
      Process.exit(pid, :kill)
      
    end
    {:noreply, state}
  end

  def init(_args) do
    {:ok, 1}
  end

end
#============================
defmodule GossipActor do
  
  use GenServer

  def start_link({name}) do
    GenServer.start_link(__MODULE__, name, name: via_tuple(name))
  end

  def init(_name) do
    {:ok, 1}
  end

  def handle_cast({:talk, curr_node, num_nodes, topology, check,pid,controller,start_time, convergence}, state) do
    neighbor = getRandomNeighbor(Topology.getNeighbors(curr_node, num_nodes, topology))

    if state < 10 do
      GossipActor.rumor(neighbor, num_nodes, topology,pid,controller,start_time, convergence)
    else 
      Terminator.check_termination(pid, num_nodes,controller,start_time, convergence)
      Process.exit(self(),:kill)
    end
    new_state = if self() != check and state < 10 do  
      state + 1
    else
      state
    end
    {:noreply, new_state}
  end


  def getRandomNeighbor(neighbors) do
    neighbor = :rand.uniform(length(neighbors))    
    neighbor = Enum.at(neighbors, neighbor - 1)
    neighbor
  end

  def rumor(curr_node, num_nodes, topology,pid, controller,start_time, convergence) do
    GenServer.cast(via_tuple(curr_node), {:talk, curr_node, num_nodes, topology, self(),pid, controller,start_time, convergence})
    GenServer.cast(self(), {:talk, curr_node, num_nodes, topology, self(),pid, controller,start_time, convergence})   
  end


  defp via_tuple(name) do
    {:via, :gproc, {:n, :l, {:name, name}}}
  end

end


#=============================================
defmodule PushSumActor do
  
  use GenServer

  def start_link({name}) do
    args = [name, 1]
    GenServer.start_link(__MODULE__, args, name: via_tuple(name))
  end

  def init(args) do
    {:ok, args}
  end

  def handle_cast({:msg, curr_node, num_nodes, topology, _check,pid,controller,start_time, value, convergence}, state) do
    [n, w] = state 
    [newN, newW] = value
    [newN, newW] = [newN + n, newW + w]
    neighbor = getRandomNeighbor(Topology.getNeighbors(curr_node, num_nodes, topology))
    if (abs((n/w)-(newN/newW))) > :math.pow(10, -10) or value == [0,0] do
      value = [newN/2, newW/2] 
      PushSumActor.rumor(neighbor, num_nodes, topology, pid, controller, start_time, value, convergence)     
      else
        Terminator.check_termination(pid, num_nodes,controller,start_time, convergence)
        Process.exit(self(),:kill)
    end
    nstate = if pid == self() do
      [n, w]
    else
      [newN/2, newW/2]    
    end
    {:noreply, nstate}
  end
 
  def rumor(curr_node, num_nodes, topology,pid, controller,start_time, value, convergence) do
    GenServer.cast(PushSumActor.via_tuple(curr_node), {:msg, curr_node, num_nodes, topology, self(),pid, controller,start_time, value, convergence})
    GenServer.cast(self(), {:msg, curr_node, num_nodes, topology, self(),pid, controller,start_time, [0, 0], convergence})   
  end

  def getRandomNeighbor(neighbors) do
    neighbor = :rand.uniform(length(neighbors))    
    neighbor = Enum.at(neighbors, neighbor - 1)
    neighbor
  end

  def via_tuple(name) do
    {:via, :gproc, {:n, :l, {:name, name}}}
  end

end

#=====================================================
defmodule Topology do

  def getNeighbors(curr_node, num_nodes, topology) do

    neighbors = case topology do
      "full" ->
        getFullNeighbors(num_nodes)
      "rand2D" ->
        dimension = :math.sqrt(num_nodes)
        dimension = round(dimension)
        getRand2DNeighbors(curr_node, dimension, num_nodes)
      "3D" ->
        dimension = :math.pow(num_nodes, 0.33)
        dimension = :math.ceil(dimension)
        dimension = round(dimension)
        get3DNeighbors(curr_node, dimension, num_nodes)
      "line" ->
        getLineNeighbors(curr_node, num_nodes)
      "impLine" ->
        getImpLineNeighbors(curr_node, num_nodes)
      "torus" ->
        dimension = :math.sqrt(num_nodes)
        dimension = round(dimension) 
        getTorusNeighbors(curr_node, dimension, num_nodes)
      _ ->
        IO.puts "Invalid topology"
    end
    neighbors

  end

  def getFullNeighbors(num_nodes) do
    range = 1..num_nodes
    neighbors = Enum.map(range, fn(i) -> i end)
    neighbors   
  end

  def getRand2DNeighbors(curr_node, dimension, num_nodes) do
    neighbors = []
    neighbors =  if((curr_node - dimension) > 0) do
      neighbors ++ [curr_node - dimension]       
    else
      neighbors
    end
    neighbors =  if((curr_node + dimension) <= num_nodes) do
     neighbors ++ [curr_node + dimension]       
    else
      neighbors
    end
    neighbors =  if(rem(curr_node - 1, dimension) != 0) do
      neighbors ++ [curr_node - 1]        
    else
      neighbors
    end
    neighbors =  if(rem(curr_node, dimension) != 0) do
      neighbors ++ [curr_node + 1]        
    else
      neighbors
    end
    neighbors
  end

  def get3DNeighbors(curr_node, dimension, num_nodes) do
    neighbors = []
    neighbors =  if((curr_node - dimension) > 0) do
      neighbors ++ [curr_node - dimension]       
    else
      neighbors
    end     
    neighbors =  if((curr_node + dimension) <= num_nodes) do
      neighbors ++ [curr_node + dimension]       
    else
      neighbors
    end     
    neighbors =  if(rem(curr_node - 1, dimension) != 0) do
      neighbors ++ [curr_node - 1]      
    else
      neighbors
    end
    neighbors =  if(rem(curr_node, dimension) != 0) do
      neighbors ++ [curr_node + 1]        
    else
      neighbors
    end     
    neighbors =  if((curr_node - (dimension*dimension)) > 0) do
      neighbors ++ [curr_node - dimension*dimension]       
    else
      neighbors
    end     
    neighbors =  if((curr_node + dimension*dimension) <= num_nodes) do
      neighbors ++ [curr_node + dimension*dimension]       
    else
      neighbors
    end
    neighbors
  end

  def getLineNeighbors(curr_node, num_nodes) do
    neighbors = []
    neighbors = if (curr_node - 1  > 0 ) do
      neighbors ++ [curr_node - 1]       
    else
      neighbors
    end
    neighbors = if (curr_node + 1  <= num_nodes ) do
      neighbors ++ [curr_node + 1]       
    else
      neighbors
    end
    neighbors    
  end

  def getImpLineNeighbors(curr_node, num_nodes) do
    neighbors = []
    neighbors = if (curr_node - 1  > 0) do
      neighbors ++ [curr_node - 1]       
    else
      neighbors
    end
    neighbors = if (curr_node + 1  <= num_nodes) do
      neighbors ++ [curr_node + 1]       
    else
      neighbors
    end
    neighbors = neighbors ++ [:rand.uniform(num_nodes)]
    neighbors    
  end

  def getTorusNeighbors(curr_node, dimension, num_nodes) do
    neighbors = []
    neighbors = if((curr_node - dimension) > 0) do
      neighbors ++ [curr_node - dimension]
    else
      neighbors ++ [num_nodes - (dimension - curr_node)]
    end
    neighbors = if((curr_node + dimension) <= num_nodes) do
      neighbors ++ [curr_node + dimension]       
    else
      neighbors ++ [dimension - (num_nodes - curr_node)]
    end     
    neighbors = if(rem(curr_node - 1, dimension) != 0) do
      neighbors ++ [curr_node - 1]        
    else
      neighbors ++ [curr_node + dimension - 1]       
    end     
    neighbors = if(rem(curr_node, dimension) != 0) do
      neighbors ++ [curr_node + 1]        
    else
      neighbors ++ [curr_node - dimension + 1]       
    end     
    neighbors    
  end
  
end