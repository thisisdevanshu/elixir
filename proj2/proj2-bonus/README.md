# Proj2

-----------------------------------------------------------------------------

Group Members:

Arnav Aima - 99864675
Devanshu Singh - 65131155

Gossip annd Push-Sum algorithm working.
Largest network:
Gossip: 4000
Push-Sum: 800

Implementation details in report

-------------------------------------------------------------------------------

Code execution:

  mix deps.get
  mix compile
  mix run proj2.exs num_nodes topology algorithm

