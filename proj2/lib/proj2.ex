defmodule Proj2 do
  
  use Application
  use Supervisor

  def start(_type,numNodes) do
    range = 1..numNodes
    children = Enum.map(range, fn(i) ->
      worker(Actor, [{i}], [ id: i, restart: :transient])
    end)
    Supervisor.start_link(children, [strategy: :one_for_one,max_restarts: 1])
    Actor.talk(1,numNodes)
  end

  def init(n) do
    {:ok, n}
  end

end

defmodule Actor do
  
  use GenServer
  def start_link({name}) do
    GenServer.start_link(__MODULE__, name, name: via_tuple(name))
  end

  def init(name) do
    {:ok, name}
  end

  def talk(pid,numNodes) do
    IO.puts pid
    GenServer.cast(via_tuple(pid), {:talk,numNodes} )
  end

  def handle_call(:talk, _, state) do
    {:reply, state, state}
  end

  def handle_cast({:talk,numNodes}, state) do
    neighbor = getNeighbor(numNodes)
    IO.puts neighbor
    if state < 10 do
     Actor.talk(neighbor, numNodes)
    end
    {:noreply, state+1}
  end

  def getNeighbor(numNodes) do
    rand_number = :rand.uniform(numNodes)
    rand_number
  end

  defp via_tuple(name) do
    {:via, :gproc, {:n, :l, {:name, name}}}
  end
end

