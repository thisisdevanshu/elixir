defmodule HelloWeb.HelloController do
  use HelloWeb, :controller


  def index(conn, _params) do
    render(conn, "show.html")
  end

  #def show(conn, %{"messenger" => messenger}) do
  #render(conn, "show.html", messenger: messenger)
  #end

  def data(conn, _params) do
  #IO.puts Hello.Application.get_data()
  n = 100
    data = Poison.encode!(%{
            "1": :rand.uniform(n),"2": :rand.uniform(n),"3": :rand.uniform(n),
            "4": :rand.uniform(n),"5": :rand.uniform(n),"6": :rand.uniform(n),
            "7": :rand.uniform(n),"8": :rand.uniform(n),"9": :rand.uniform(n),
            "10": :rand.uniform(n),"11": :rand.uniform(n),"12": :rand.uniform(n),
            "13": :rand.uniform(n),"14": :rand.uniform(n),"15": :rand.uniform(n),
            "16": :rand.uniform(n),"17": :rand.uniform(n),"18": :rand.uniform(n),
            "19": :rand.uniform(n),"20": :rand.uniform(n),"21": :rand.uniform(n),
            "22": :rand.uniform(n),"23": :rand.uniform(n),"24": :rand.uniform(n),
            "25": :rand.uniform(n),"26": :rand.uniform(n),"27": :rand.uniform(n),
            "28": :rand.uniform(n),"29": :rand.uniform(n),"30": :rand.uniform(n),
            "31": :rand.uniform(n),"32": :rand.uniform(n),"33": :rand.uniform(n),
            "34": :rand.uniform(n),"35": :rand.uniform(n),"36": :rand.uniform(n),
            "37": :rand.uniform(n),"38": :rand.uniform(n),"39": :rand.uniform(n),
            "40": :rand.uniform(n),"41": :rand.uniform(n),"42": :rand.uniform(n),
        })
    

    #:timer.sleep(30000)
    job(conn)

    
  end

    def job(conn) do
        [pending_transactions, transactions, network, wallets] = GenServer.call(:transactor, :get_data )
        {id,pid} = Map.fetch(network, 1)
        blockchain = GenServer.call(pid, :get_blockchain)
        b = format_block(3, blockchain, %{})
        d = format_wallet(0,wallets, %{})
        
        #IO.inspect data
        b = Poison.encode!(b)
        d = Poison.encode!(d)
        {:ok, {hash, time , nonce, transactions} } = Enum.fetch(blockchain, length(blockchain)-1)
        {:ok, {prev_hash, prev_time , prev_nonce, data} } = Enum.fetch(blockchain, length(blockchain)-2)
        list = Map.to_list(wallets)
        {:ok, {public, pid}} = Enum.fetch(list, 0)
        #data = data ++ b
        #data = data ++ d
        IO.inspect data
        #data = Poison.encode!(data)
        render(conn, "show.html", %{b: b, d: d, hash: hash, time: time, prev_hash: prev_hash, 
        nonce: nonce, transactions: transactions, balance: GenServer.call(pid, :get_balance),  public: public, 
        private: GenServer.call(pid, :get_private_key)})
        
        #:timer.sleep(1000)
        #job(conn, transactor)
    end

    def format_wallet(i, wallets, data) do
        list = Map.to_list(wallets)
        if i < 10 do
         {:ok, {id, pid}} = Enum.fetch(list, i)
         #IO.inspect id
         #IO.inspect pid
        updated_map = Map.put(data,i, GenServer.call(pid, :get_balance))
        updated_map = format_wallet(i+1, wallets, updated_map)
        updated_map
      else
        data
      end
    end

    def format_block(i, blockchain, data) do
        if i < length(blockchain) do
         {:ok, {hash, time , nonce, new_data} } = Enum.fetch(blockchain, i)
         {:ok, {hash_o, time_o , nonce_o, new_data_o} } = Enum.fetch(blockchain, i-1)
         #IO.inspect id
         #IO.inspect pid
       
        updated_map = Map.put(data,i,String.to_integer(time)-String.to_integer(time_o) )
        updated_map = format_block(i+1, blockchain, updated_map)
        updated_map
        else
            data
        end
    end
 

    #def chart(:column, data), do: chart("Column", data)
  #def chart(:bar, data), do: chart("Bar", data)
  #def chart(:pie, data), do: chart("Pie", data)
  #def chart(:line, data), do: chart("Line", data)
  #def chart(:area, data), do: chart("Area", data)
  #def chart(:scatter, data), do: chart("Scatter", data)

  #def chart(data) do
    #data = Poison.encode!([[175, 60], [190, 80], [180, 75]])
    #render(conn, "show.html", data: data)
    #data = Poison.encode!(data)
    #raw """
    #  <div id="chart-#{klass}" style="height:300px">Loading..</div>
    #  <script>
    #    new Chartkick.#{klass}Chart('chart-#{klass}', #{data});
    #  </script>
    #"""
  #end
end