# Bitcoin

Team Members
--------------------------------
1. Arnav Aima
2. Devanshu Singh

Functionalities implemented
--------------------------------
1. A network of nodes start with the genesis block and one of them successfully generates the first block and 25 bitcoins come into existence.
2. The mined bitcoins are transferred to wallet owned by the node.
3. A wallet has its own public key and private key and is independent of any node of the blockchain.
4. Each wallet is identified by its public key.
5. To transact bitcoins the sender wallet signs the transaction with their private key, includes the amount and the receiver's public key.
6. The transactions are then send to the blockchain network and once the transaction is included in a block the sender's and receiver's wallets are updated.
7. The transactions data becomes part of the block and can be referred whenever required.

Block Structure : {previous_hash, hash, timestamp, nonce, transactions_list}

Transaction Structure : {signature, sender_public_key, receiver_public_key, amount}

Bonus part
--------------------------------
1. We have created the full network of blockchain and every transaction is send to the nodes that are competing among themselves to generate the next block.
2. We also taken care of the double spending problem where a wallet spends the same money twice. Our network stops such transactions from happening.
3. We have used public private key for creating wallets and each transaction is signed by the senders private key.

How to run the project ?
---------------------------------
mix deps.get
mix compile
mix test

The above commands run the following test cases.
----------------------------------
1. Bitcoin mining test - New blocks are generated and 25 BTC are created. The creation of 25 BTC is a transaction signed by the next block.
2. Generated Block Validation test - Generated blocks are broadcasted & other nodes accept the new block into the blockchain after validating the new block.
3. Valid Transaction test - Here a sender wallet sends some money to a receiver using the blockchain network.
4. Insufficient balance transaction test - A wallet with insufficient balance is stopped from transacting.
5. Double spending test - A wallets tries to double spend and only of the transaction is accepted in blockchain while the other transaction is rejected.


Note: The test cases are run on a machine with 8GB RAM and Pentium i5 processor. The difficulty level is set to 5 for mining bitcoins and may take upto 10 seconds.

