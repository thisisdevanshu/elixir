defmodule BitcoinTest do
  use ExUnit.Case
  doctest Bitcoin


 #bitcoin  mining
  test "bitcoin mining" do
  	IO.puts "    "
  	IO.puts "2. =============== TESTING BITCOIN MINING ==============="
  	IO.puts "    "
  	{:ok, transactor} = Transaction.start_link()
	map = %{}
	map = Proj4.initiate_wallets(1,4,map,transactor)
	Bitcoin.start(transactor, map, 4)
	:timer.sleep(30000)
  end


  #block validation
  test "block validation" do
  	IO.puts "    "
  	IO.puts "3. =============== TESTING BLOCK VALIDATION ==============="
  	IO.puts "    "
  	previous_hash = "000001562421835D1F26064CEC9883AFDEEEE126C2CCECF25967447120808DF1"
  	IO.inspect previous_hash, label: "Previous hash : "
 	{hash, time, nonce, data}= {"000008B58A0DDC28E74272A319B3B2D85B4B98C329ACF4442C38633D497B5958","1543173061509", 822554, []}
 	block = {hash, time, nonce, data}
 	IO.inspect block,label: "Generated block"
 	isValid = Block.is_valid(hash, time, previous_hash, data, nonce)
 	IO.puts "Is Block Valid ? #{isValid}"

  end

  #valid transaction
  test "Valid transaction" do
  	IO.puts "    "
  	IO.puts "1. =============== TESTING VALID TRANSACTION ==============="
  	IO.puts "    "

  	{:ok, transactor} = Transaction.start_link()
	map = %{}
	map = Proj4.initiate_wallets(1,4,map,transactor)
	Bitcoin.start(transactor, map, 4)
	
	{:ok, sender} = Map.fetch(map,1)
	GenServer.cast(sender, {:set_balance, 30})
	{:ok, receiver} = Map.fetch(map,2)
	GenServer.cast(sender, {:set_balance, 24})
	IO.inspect "#{GenServer.call(sender, :get_balance)} BTC", label: "Sender's balance : "
	IO.inspect "#{GenServer.call(receiver, :get_balance)} BTC", label: "Receiver's balance : "
	IO.puts "Sending 10 BTC..."
	recipient = GenServer.call(receiver, :get_public_key)
	#:timer.sleep(10000)#
	GenServer.cast(sender, {:send, recipient, 10})
	:timer.sleep(20000)
	IO.inspect "#{GenServer.call(sender, :get_balance)} BTC", label: "Sender's balance : "
	IO.inspect "#{GenServer.call(receiver, :get_balance)} BTC", label: "Receiver's balance : "
  end

  #insuffucient balance
  test "insuffucient balance transaction" do
  	IO.puts "    " 
  	IO.puts "4. =============== TESTING INSUFFUCIENT BALANCE ==============="
  	IO.puts "    "
  	{:ok, transactor} = Transaction.start_link()
	map = %{}
	map = Proj4.initiate_wallets(1,4,map,transactor)
	Bitcoin.start(transactor, map, 4)
	#:timer.sleep(30000)
	{:ok, sender} = Map.fetch(map,1)
	GenServer.cast(sender, {:set_balance, 7})
	{:ok, receiver} = Map.fetch(map,2)
	GenServer.cast(receiver, {:set_balance, 14})

	IO.inspect "#{GenServer.call(sender, :get_balance)} BTC", label: "Sender's balance : "
	IO.inspect "#{GenServer.call(receiver, :get_balance)} BTC", label: "Receiver's balance : "
	IO.puts "Sending 10 BTC..."
	recipient = GenServer.call(receiver, :get_public_key)
	GenServer.cast(sender, {:send, recipient, 10})
	:timer.sleep(3000)
  end

  test "double spending" do
  	IO.puts "    "
  	IO.puts "5. =============== TESTING DOUBLE SPENDING ==============="
  	IO.puts "    "

  	{:ok, transactor} = Transaction.start_link()
	map = %{}
	map = Proj4.initiate_wallets(1,5,map,transactor)
	Bitcoin.start(transactor, map, 4)
	
	{:ok, sender} = Map.fetch(map,4)
	GenServer.cast(sender, {:set_balance, 10})

	{:ok, receiver1} = Map.fetch(map,2)
	

	{:ok, receiver2} = Map.fetch(map,3)
	

	IO.inspect "#{GenServer.call(sender, :get_balance)} BTC", label: "Sender's balance : "
	IO.inspect "#{GenServer.call(receiver1, :get_balance)} BTC", label: "Receiver1's balance : "
	IO.inspect "#{GenServer.call(receiver2, :get_balance)} BTC", label: "Receiver2's balance : "

	IO.puts "Sending 10 BTC to Receiver1..."
	IO.puts "Sending 10 BTC to Receiver2..."

	recipient1 = GenServer.call(receiver1, :get_public_key)
	recipient2 = GenServer.call(receiver2, :get_public_key)
	#:timer.sleep(10000)
	GenServer.cast(sender, {:send, recipient1, 10})
	:timer.sleep(9000)
	GenServer.cast(sender, {:send, recipient2, 10})
	:timer.sleep(20000)

	IO.inspect "#{GenServer.call(sender, :get_balance)} BTC", label: "Sender's balance : "
	IO.inspect "#{GenServer.call(receiver1, :get_balance)} BTC", label: "Receiver1's balance : "
	IO.inspect "#{GenServer.call(receiver2, :get_balance)} BTC", label: "Receiver2's balance : "

  end





end
